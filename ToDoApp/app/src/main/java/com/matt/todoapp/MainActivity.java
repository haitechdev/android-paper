package com.matt.todoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList items;
    private ArrayAdapter<String> itemsAdaptor;
    private ListView lvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvItems  =(ListView) findViewById(R.id.listItems);
        items = new ArrayList<String>();
        itemsAdaptor = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        lvItems.setAdapter(itemsAdaptor);
        //items.add("First Item");
        //items.add("Second Item");
        loadItems();

        setupListViewListener();
    }

    public void onAddItem(View v){
        EditText etNewItem = (EditText) findViewById(R.id.etNewItem);
        String itemText = etNewItem.getText().toString();
        itemsAdaptor.add(itemText);
        etNewItem.setText("");

        saveItems();
    }

    //Attaches a long click listener to the listview
    private void setupListViewListener(){
        lvItems.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener(){
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter,
                                                   View item, int pos, long id){
                        //Remove the item within array at position
                        items.remove(pos);
                        //Refresh the adaptor
                        itemsAdaptor.notifyDataSetChanged();
                        //Return true consumes the long click event (marks it handled)
                        return true;
                    }
                }
        );
    }

    //*Save To Do Items to file
    private void saveItems(){
        PrintWriter writer = null;
        try{
            //Create the output stream and assign the name of the text file
            FileOutputStream fos = openFileOutput("todo.txt", MODE_PRIVATE);
            //write the data to the stream
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    fos)));
            //make sure we write until the end
            for(int idx = 0; idx < items.size(); idx++){
                writer.println(items.get(idx));
            }
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally {
            if(null != writer){
                writer.close();
            }
        }
    }

    /* Load stored To Do Items */
    private void loadItems(){
        /*Create the buffered reader*/
        BufferedReader reader = null;
        try{
            /*create the filestream and open the file*/
            FileInputStream fis = openFileInput("todo.txt");
            /*read the data*/
            reader = new BufferedReader(new InputStreamReader(fis));
            /*set up the variables*/
            String title = null;
            /*extract the data from the buffer and assign to variables*/
            while (null != (title = reader.readLine())){
                //add the to do item
                items.add(title);
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            //force close
            if(null != reader){
                try{
                    reader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
